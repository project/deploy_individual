CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

This module uses the deploy ecosystem to allow to share specific piece of
content.

REQUIREMENTS
------------

This module requires the following modules:
 * Workspace (https://www.drupal.org/project/workspace)

This module requires no additional library.

INSTALLATION
------------

 * Install and enable this module like any other drupal 8 module.

CONFIGURATION
-------------

 * Same configuration as Deploy/Workspace.

MAINTAINERS
-----------

This project has been sponsored by:
 * Smile - http://www.smile.fr
